'''

 ZADANIE 3. NALEŻY WYGENEROWAĆ LISTĘ LICZB OD 2 DO 5.5 ZE SKOKIEM CO 0.5, DANE WYNIKOWE MUSZĄ BYĆ TYPU DECIMAL.

'''
from decimal import *

def listaZakresemIskokiem(start,stop):

    v = (stop - start) / 0.5
    home = []
    v = int(v)
    f=0.0

    for g in range(v+1):
        f= f + 0.5
        home.append(Decimal(f+start-0.5))

    return home


wynik = listaZakresemIskokiem(2,5.5)
print(wynik)
