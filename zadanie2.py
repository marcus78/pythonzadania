'''


 ZADANIE 2. PODANA JEST LISTA ZAWIERAJĄCA ELEMENTY O WARTOŚCIACH 1-n. NAPISZ FUNKCJĘ KTÓRA SPRAWDZI JAKICH ELEMENTÓW BRAKUJE

1-n = [1,2,3,4,5,...,10]
np. n=10
wejście: [2,3,7,4,9], 10
wyjście: [1,5,6,8,10]


'''


def missingElements(n, input):
    base = list(range(1,n+1))

    for t in input:
        if t in base:
            base.remove(t)
    return base

print(missingElements(7,[1,3,5,2]))