'''
 ZADANIE 1. GENERATOR KODÓW POCZTOWYCH

przyjmuje 2 stringi: "79-900" i "80-155" i zwraca listę kodów pomiędzy nimi

'''
short = []
long = []
output = []
output1 = []
final = []
outputMid = []
outputZero = []
outputOne = []


def middleList(p1, p2):
    base = []

    for a in range(p1 + 1, (p2 - p1) + p1):
        for b in range(1000):
            if b < 10:
                base.append(str(a) + "-" + "00" + str(b))
            elif b >= 10 and b <= 99:
                base.append(str(a) + "-" + "0" + str(b))
            else:
                base.append(str(a) + "-" + str(b))
    return base


def zeroMode(b, p1, p2):
    for x in range(p1 + 1, (p2 - p1) + p1):
        if x < 10:
            output1.append(str(b) + "-" + "00" + str(x))
        elif x >= 10 and x <= 99:
            output1.append(str(b) + "-" + "0" + str(x))
        else:
            output1.append(str(b) + "-" + str(x))

    return output1


def differentOne(p1, p2, q1, q2):

    for x in range(p2 + 1, 1000):
        if x < 10:
            output.append(str(p1) + "-" + "00" + str(x))
        elif x >= 10 and x <= 99:
            output.append(str(p1) + "-" + "0" + str(x))
        else:
            output.append(str(p1) + "-" + str(x))

    for y in range(q2):
        if y < 10:
            output.append(str(q1) + "-" + "00" + str(y))

        elif y >= 10 and y <= 99:
            output.append(str(q1) + "-" + "0" + str(y))
        else:
            output.append(str(q1) + "-" + str(y))

    return output

# MAIN FUNCTION

def postcodesGenerator(postCodeOne, postCodeTwo):

    firstOne = int(postCodeOne[0] + postCodeOne[1])
    secondOne = int(postCodeOne[3] + postCodeOne[4] + postCodeOne[5])

    firstTwo = int(postCodeTwo[0] + postCodeTwo[1])
    secondTwo = int(postCodeTwo[3] + postCodeTwo[4] + postCodeTwo[5])

    short.append(firstOne)
    short.append(firstTwo)

    long.append(secondOne)
    long.append(secondTwo)

    switch = firstTwo - firstOne

    if switch == 1:
        outputOne = differentOne(firstOne, secondOne, firstTwo, secondTwo)
        return outputOne

    elif switch > 1:
        outputMid = middleList(firstOne, firstTwo)
        outputOne = differentOne(firstOne, secondOne, firstTwo, secondTwo)
        return sorted(outputMid+outputOne)

    elif switch == 0:
        outputZero = zeroMode(firstOne, secondOne, secondTwo)
        return outputZero

print(postcodesGenerator("22-997", "23-004"))
